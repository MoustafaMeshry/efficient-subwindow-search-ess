% This function extracts interest points of an image and maps them to features of the codebook, it returns the same format of the .clst files required by the ESS branch and bound code
% imagePath: path to the target image
% codebook: codebook of representative visual words
% kdtree: constructed kd-tree used to efficiently mapping image features to visual words in the codebook
% @return: retunrs the coordicnates of each interest point and the codebook featureId to which it was mapped to. This is the same format of the *.clst file required by the ESS branch and bound code
function [x, y, featureId] = getImageClusters(imagePath, left, top, right, bottom, isInsideBoxes, codebook, kdtree)
%	img = imread(imagePath);
%	ipts = OpenSurf(img);

	ipts = extractImageFeatures(imagePath, left, top, right, bottom, isInsideBoxes);
	numIpts = length(ipts);
	x = zeros(numIpts, 1);
	y = zeros(numIpts, 1);
	featureId = zeros(numIpts, 1);
	
	for i=1:numIpts
		%id = findClosestMatch(ipts(i).descriptor, codebook, kdtree);
		id = findClosestMatch2(ipts(i).descriptor, codebook);
		x(i) = ipts(i).x;
		y(i) = ipts(i).y;
		featureId(i) = id;
	end

end

% This function maps an interest point (described by it SURF descriptor) to the best match of the visual words in the codebook (represented by kd-tree for effecient matching)
% targetDescriptor: Descriptor of the target feature
% codebook: codebook of representative visual words
% tree: constructed kd-tree used to efficiently mapping image features to visual words in the codebook
% @return: returns the codebook featureId to which the descriptor was mapped to
function featureId = findClosestMatch(targetDescriptorT, codebook, tree)
	global DESCRIPTOR_SIZE

	targetDescriptor = targetDescriptorT';
	featureId = tree(1);
	treeIndex = 1;
	compDimension = 1;
	minSse = 1000000000000000000;
	
	while treeIndex <= length(tree) && tree(treeIndex) ~= -1
		sse = sumSquaredError(targetDescriptor, codebook(tree(treeIndex), :));
		if sse < minSse
			minSse = sse;
			featureId = tree(treeIndex);
		end
		
		comp = compareDescriptors(targetDescriptor, codebook(tree(treeIndex),:), compDimension);
		if comp <= 0
			treeIndex = treeIndex*2;
		else
			treeIndex = (treeIndex*2) + 1;
		end
		
		compDimension = compDimension+1;
		if(compDimension > DESCRIPTOR_SIZE)
			compDimension = 1;
		end
	end
end

% This function maps an interest point (described by it SURF descriptor) to the best match of the visual words in the codebook (by brute-forcing on all codebook features and minimizing the sum of squared error)
% targetDescriptor: Descriptor of the target feature
% codebook: codebook of representative visual words
% tree: constructed kd-tree used to efficiently mapping image features to visual words in the codebook
% @return: returns the codebook featureId to which the descriptor was mapped to
function featureId = findClosestMatch2(targetDescriptor, codebook)
	featureId = 1;
	minSse = sumSquaredError(targetDescriptor, codebook(1,:));
	
	for i=2:size(codebook, 1)
		sse = sumSquaredError(targetDescriptor, codebook(i,:));
		if(sse < minSse)
			minSse = sse;
			featureId = i;
		end
	end
end

% this function calculates the sum of squared error between two descriptors
% descriptor1: the first descriptor
% descriptor2: the second descriptor
% @return: returns the sum of squared error between the two descriptors
function sse = sumSquaredError(descriptor1, descriptor2)
	sse = 0;
	for i=1:length(descriptor1)
		sse = sse + (descriptor1(i) - descriptor2(i))^2;
	end
end

% this function compares two descriptors by a given dimension
% desc1: the first descriptor
% desc2: the second descriptor
% compDimension: the dimension which will be used to compare the two descriptors
function comp = compareDescriptors(desc1, desc2, compDimension)
	EPS = 1e-10;
	comp = 0;
	if desc1(compDimension) > desc2(compDimension) + EPS
		comp = 1;
	elseif desc1(compDimension) + EPS < desc2(compDimension)
		comp = -1;
	end
end


