% this function wraps and visualizes the ESS c++ code
% it collects its output and draws the resulting bounding box(es) on the given image:
% Input Arguments:
% options: command line options of the command that runs the ESS c++ code
% imgPath: path to the test image
% weightPath: path to the *.weight file for the test image
% dataPath: path the data file (*.clst) for the test image

function result = ess(options, imgPath, weightPath, dataPath)
	result = ess_wrapper(options, imgPath, weightPath, dataPath);
	score  = result(1);
	left   = result(2);
	top    = result(3);
	right  = result(4);
	bottom = result(5);
	
%	return;
	[score, left, top, right, bottom]
	img = imread(imgPath);
	imshow(img);
	for i = 1:length(left)
		width = right(i)-left(i);
		height = bottom(i)-top(i);
		if width==0
			width=1;
		end
		if height==0
			height=1;
		end
		rectangle('Position',[left(i), top(i), width, height], 'LineWidth', 2 ,'EdgeColor' ,'b');
%		rectangle('Position',[top(i), left(i), height, width], 'LineWidth', 2 ,'EdgeColor' ,'b');
	end
end
