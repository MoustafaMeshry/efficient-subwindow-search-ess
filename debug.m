function debug()
global model1
%%{
codebook = csvread('categories/cat/codebook.txt');
kdTreeRaw = textread('categories/cat/kdTree.txt', '%d');
kdTree = kdTreeRaw(2:length(kdTreeRaw));

left1 = 11;
top1 = 135;
right1 = 333;
bottom1 = 410;
iptsPos = extractImageFeatures('sampleImages/1.png', left1, top1, right1, bottom1, true);
iptsNeg = extractImageFeatures('sampleImages/1.png', left1, top1, right1, bottom1, false);
cat1ImgPath = 'sampleImages/1.png';
cat1WeightPath = 'categories/cat/cat.weight';
cat1DataPath = 'sampleImages/1.clst';
hPos = getImageHistogram('sampleImages/1.png', left1, top1, right1, bottom1, true, codebook, kdTree);
hNeg = getImageHistogram('sampleImages/1.png', left1, top1, right1, bottom1, false, codebook, kdTree);
trainingHists1 = [hPos';hNeg'];
labels1 = [1;-1];
model1 = svmtrain(trainingHists1, labels1);
calculateWeightsFile(model1);

cat1Weights = textread('categories/cat/cat.weight');
[x1, y1, id1] = textread('sampleImages/1.clst', '%d %d %d');

%verify +ve and -ve cluster wieghts by calculating sum of weights of features inside and outside the bounding box and comparing them!
posWeight = 0;
negWeight = 0;
for i=1:length(x1)
	if x1(i) >= left1 && x1(i) <= right1 && y1(i) >= top1 && y1(i) <= bottom1
		posWeight = posWeight + cat1Weights(id1(i)+1);
	else
		negWeight = negWeight + cat1Weights(id1(i)+1);
	end
end

posWeight
negWeight
%}

%================================================================================================================================================
%{
%verify your debugging approach by applying it to the cow .weight and .clst files

cowWeights = textread('examples/cow.weight');
[xCow, yCow, idCow] = textread('examples/cow.clst', '%d %d %d');
posWeight = 0;
negWeight = 0;

leftCow = 89;
topCow = 117;
rightCow = 258;
bottomCow = 209;

cow_weight = fopen('examples/cow_weight.txt', 'w');
cow_clst = fopen('examples/cow_clst.txt', 'w');
cow_pos_features = fopen('examples/cow_pos_features.txt', 'w');
cow_neg_features = fopen('examples/cow_neg_features.txt', 'w');

for i=1:length(xCow)
	fprintf(cow_clst, '%d %d %d\n', xCow(i), yCow(i), idCow(i));
	
	if xCow(i) >= leftCow && xCow(i) <= rightCow && yCow(i) >= topCow && yCow(i) <= bottomCow
		posWeight = posWeight + cowWeights(idCow(i)+1);
		fprintf(cow_pos_features, '%d: %d %g\n', i, idCow(i), cowWeights(idCow(i)));
	else
		negWeight = negWeight + cowWeights(idCow(i)+1);
		fprintf(cow_neg_features, '%d: %d %g\n', i, idCow(i), cowWeights(idCow(i)));
	end
end

for i=1:length(cowWeights)
	fprintf(cow_weight, '%.10g\n', cowWeights(i));
end

fclose(cow_weight);
fclose(cow_clst);
fclose(cow_pos_features);
fclose(cow_neg_features);

posWeight
negWeight

%iptsPos = extractImageFeatures('examples/cow.jpg', leftCow, topCow, rightCow, bottomCow, true);
%iptsNeg = extractImageFeatures('examples/cow.jpg', leftCow, topCow, rightCow, bottomCow, false);
%PaintSURF(imread('examples/cow.jpg'), iptsPos);
%PaintSURF(imread('examples/cow.jpg'), iptsNeg);
%}

%=============================================================================================================

%{
codebook = csvread('categories/cat/codebook.txt');
kdTreeRaw = textread('categories/cat/kdTree.txt', '%d');
kdTree = kdTreeRaw(2:length(kdTreeRaw));

left43 = 285;
top43 = 8;
right43 = 500;
bottom43 = 333;
iptsPos = extractImageFeatures('sampleImages/43.png', left43, top43, right43, bottom43, true);
iptsNeg = extractImageFeatures('sampleImages/43.png', left43, top43, right43, bottom43, false);
cat43ImgPath = 'sampleImages/43.png';
cat43WeightPath = 'categories/cat/cat.weight';
cat43DataPath = 'sampleImages/43.clst';
hPos = getImageHistogram('sampleImages/43.png', left43, top43, right43, bottom43, true, codebook, kdTree);
hNeg = getImageHistogram('sampleImages/43.png', left43, top43, right43, bottom43, false, codebook, kdTree);
trainingHists43 = [hPos';hNeg'];
labels43 = [1;-1];
model1 = svmtrain(trainingHists43, labels43);
calculateWeightsFile(model1);

cat43Weights = textread('categories/cat/cat.weight');
[x43, y43, id43] = textread('sampleImages/43.clst', '%d %d %d');

%verify +ve and -ve cluster wieghts by calculating sum of weights of features inside and outside the bounding box and comparing them!
posWeight = 0;
negWeight = 0;
for i=1:length(x43)
	if x43(i) >= left43 && x43(i) <= right43 && y43(i) >= top43 && y43(i) <= bottom43
		posWeight = posWeight + cat43Weights(id43(i)+1);
	else
		negWeight = negWeight + cat43Weights(id43(i)+1);
	end
end

posWeight
negWeight
%}
end
