import java.io.*;
import java.util.*;

public class KdTreeBuilder {
	public static final String CATEGORY = "cat";
	public static final String CODEBOOK_FILE_NAME = "categories/" + CATEGORY +  "/codebook.txt";
	public static final int DESCRIPTOR_SIZE = 64;
	public static final int CODEBOOK_SIZE = 1000;
	private int descriptorId = 1;
	
	private void constructTree(KdTree tree, Node[] nodes, int level, int st, int end){
		if(st>end)
			return;
		
		NodeComparator comparator = new NodeComparator(level%DESCRIPTOR_SIZE);
		Arrays.sort(nodes, st, end, comparator);
		int median = (st+end)>>1;
		
		tree.insert(nodes[median]);
		constructTree(tree, nodes, level+1, st, median-1);
		constructTree(tree, nodes, level+1, median+1, end);
	}
	
	public KdTree buildKdTree() throws IOException{
		BufferedReader bf = new BufferedReader(new FileReader(CODEBOOK_FILE_NAME));
		descriptorId = 1;
		String line = null;

		Node[] arr = new Node[CODEBOOK_SIZE];
		
		for(int i=0; i<arr.length; i++){
			line = bf.readLine();
			if(line == null){
				System.err.println("codebook size is larger than input file size!");
				System.exit(1);
			}
			
			arr[i] = new Node(descriptorId++, line);
		}
		bf.close();
		
		KdTree tree = new KdTree();
		constructTree(tree, arr, 0, 0, arr.length-1);
		PrintWriter pw = new PrintWriter(new FileWriter("categories/" + CATEGORY + "/kdTree.txt"));
		pw.println(tree);
		pw.close();
		
		return tree;
	}
	
	public static void main(String[] args) throws IOException {
		new KdTreeBuilder().buildKdTree();
	}
}
