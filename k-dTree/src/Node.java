
public class Node {
	
	public int descriptorId;
	public double[] descriptor;
	public Node left;
	public Node right;
	
	public Node(int descriptorId, String nodeLine){
		this.descriptorId = descriptorId;
		left = right = null;
		
		String[] toks = nodeLine.split(",");
		descriptor = new double[toks.length];
		for(int i=0; i<toks.length; ++i){
			descriptor[i] = Double.parseDouble(toks[i]);
		}
	}
	
	public Node(int descriptorId, double[] descriptor){
		this.descriptorId = descriptorId;
		this.descriptor = new double[descriptor.length];
		for(int i=0; i<descriptor.length; i++)
			this.descriptor[i] = descriptor[i];
	}
}
