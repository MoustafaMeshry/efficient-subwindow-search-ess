
public class KdTree {

	public Node root;
	private NodeComparator comparator;
	private int maxLevel;
	
	public KdTree(){
		root = null;
		maxLevel = 0;
		comparator = new NodeComparator(-1);
	}
	
	private void insertAux(Node crnt, Node newborn, int level){
		if(level+1 > maxLevel)
			maxLevel = level+1;
		
		if(comparator.compare(newborn, crnt, (level-1)%crnt.descriptor.length) <= 0){
			if(crnt.left == null)
				crnt.left = newborn;
			else 
				insertAux(crnt.left, newborn, level+1);
		} else{
			if(crnt.right == null)
				crnt.right = newborn;
			else 
				insertAux(crnt.right, newborn, level+1);
		}
	}
	
	public void insert(int descriptorId, String descriptorLine){
		insert(new Node(descriptorId, descriptorLine));
	}
	
	public void insert(Node node){
		if(root == null){
			root = node;
			maxLevel = 1;
		} else{
			insertAux(root, node, 1);
		}
	}
	
	private double calculateSse(Node nd1, Node nd2){
		double err = 0;
		for(int i=0; i<nd1.descriptor.length; i++)
			err += (nd1.descriptor[i] - nd2.descriptor[i]) * (nd1.descriptor[i] - nd2.descriptor[i]);
		
		return err;
	}
	
	
	private Node findClosestMatchAux(Node target){
		if(root == null)
			return null;
		
		int compDimension = 0;
		double minSse = Double.MAX_VALUE;
		Node ret = null;
		Node crnt = root;
		NodeComparator comparator = new NodeComparator(-1);
		
		while(crnt != null){
			double sse = calculateSse(crnt, target);
			if(sse < minSse){
				minSse = sse;
				ret = crnt;
			}
			
			int cmp = comparator.compare(target, crnt, compDimension);
			if(cmp <= 0) 
				crnt = crnt.left;
			else 
				crnt = crnt.right;
			
			compDimension = (compDimension+1)%KdTreeBuilder.DESCRIPTOR_SIZE;
		}
		
		return ret;
	}
	
//	private Node findClosestMatchAux(Node crnt, Node target, int compDimension){
//		int nextDimension = (compDimension+1)%crnt.descriptor.length;
//		
//		if(comparator.compare(target, crnt, compDimension) <= 0){
//			if(crnt.left == null)
//				return crnt;
//			else
//				return findClosestMatchAux(crnt.left, target, nextDimension);
//		} else{
//			if(crnt.right == null)
//				return crnt;
//			else 
//				return findClosestMatchAux(crnt.right, target, nextDimension);
//		}
//	}
	
	public int findClosestMatch(String descriptorLine){
		Node dummy = new Node(-1, descriptorLine);
		Node parent = findClosestMatchAux(dummy);
		return parent.descriptorId;
	}
	
	public int findClosestMatch(double[] descriptor){
		Node dummy = new Node(-1, descriptor);
		Node parent = findClosestMatchAux(dummy);
		return parent.descriptorId;
	}
	
	@Override
	public String toString(){
		Node[] treeAr = new Node[(1<<(maxLevel))];
		treeAr[1] = root;
		System.out.println("maxLevel = " + maxLevel);
		System.out.println("length = " + treeAr.length);
		for(int i=1; (i<<1)<treeAr.length; i++){
			if(treeAr[i] == null) continue;
//			System.out.println(i + ": " + treeAr[i] + ", " + treeAr[i].descriptorId);
			treeAr[i<<1] = treeAr[i].left;
			treeAr[i<<1|1] = treeAr[i].right;
		}
		
		StringBuffer sb = new StringBuffer();
		//1st line is the size of the tree
		sb.append(treeAr.length - 1);
		for(int i=1; i<treeAr.length; i++){
			if(treeAr[i]==null) sb.append("\n-1");
			else sb.append("\n" + treeAr[i].descriptorId);
		}
		return sb.toString();
	}
}
