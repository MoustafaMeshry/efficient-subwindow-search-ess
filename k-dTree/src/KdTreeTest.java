import java.io.*;

public class KdTreeTest {

	void run()throws IOException{
		BufferedReader bf = new BufferedReader(new FileReader(KdTreeBuilder.CODEBOOK_FILE_NAME));
		int descriptorId = 1;
		String line = null;

		Node[] arr = new Node[KdTreeBuilder.CODEBOOK_SIZE];
		
		for(int i=0; i<arr.length; i++){
			line = bf.readLine();
			if(line == null){
				System.err.println("codebook size is larger than input file size!");
				System.exit(1);
			}
			
			arr[i] = new Node(descriptorId++, line);
		}
		bf.close();
		
		KdTree tree = new KdTreeBuilder().buildKdTree();
		
		int errCnt = 0;
		for(int i=0; i<arr.length; i++){
			int retId = tree.findClosestMatch(arr[i].descriptor);
			if(retId != arr[i].descriptorId) errCnt++;
		}

		System.out.println("Error Count = " + errCnt);
	}
	
	public static void main(String[] args)throws IOException {
		new KdTreeTest().run();
	}
}
