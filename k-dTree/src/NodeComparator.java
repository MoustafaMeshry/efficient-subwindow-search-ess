import java.util.Comparator;


public class NodeComparator implements Comparator<Node>{

	private int compDimension;
	private final double EPS = 1e-10;
	
	public NodeComparator(int compDimension){
		this.compDimension = compDimension;
	}
	
	public int getCompDimension(){
		return this.compDimension;
	}
	
	public void setCompDimension(int compDimension){
		this.compDimension = compDimension;
	}
	
	public int compare(Node node0, Node node1, int dimension){
		int old = compDimension;
		compDimension = dimension;
		int ret = compare(node0, node1);
		compDimension = old;
		return ret;
	}
	
	@Override
	public int compare(Node node0, Node node1) {
		if(node0.descriptor[compDimension] > node1.descriptor[compDimension] + EPS)
			return 1;
		else if(node0.descriptor[compDimension] + EPS < node1.descriptor[compDimension])
			return -1;
		else
			return 0;
	}
}
