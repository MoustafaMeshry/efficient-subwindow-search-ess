function calculateWeightsFile(svmModel)
	global CATEGORY CODEBOOK_SIZE

	%trainingHistograms = csvread(strcat('categories/', CATEGORY, '/trainingHistograms.txt'));	
	weightsVector = zeros(CODEBOOK_SIZE, 1);	

	for i=1:CODEBOOK_SIZE
		weightsVector(i) = 0;
		for j=1:length(svmModel.Alpha)
%		for j=1:length(svmModel.sv_coef)
%			weightsVector(i) = weightsVector(i) + svmModel.Alpha(j) * -1 * trainingHistograms(svmModel.SupportVectorIndices(j), i);
			weightsVector(i) = weightsVector(i) + svmModel.Alpha(j) * -1 * svmModel.SupportVectors(j, i);
%			weightsVector(i) = weightsVector(i) + svmModel.sv_coef(j) * svmModel.SVs(j, i);
		end
	end

	weightsFile = fopen(strcat('categories/', CATEGORY, '/', CATEGORY, '.weight'), 'w');

	for i=1:length(weightsVector)
		fprintf(weightsFile, '%g\n', weightsVector(i));
	end
	fclose(weightsFile);
end

