% This function takes a list of images names, extracts descriptors from them and exports extracted descriptors to a file
% listFileName: path of a file containing paths to images to process
% groundTruthFileName: path of the ground truth folder, this is to extract features contained only in the bounding boxes, if the file doesn't exist, then all interest points will be considered
% upperLimitToDescriptorsCount: Upper limit to number of extracted features (set it to 300,000 or so)
% outputFileName: name of the output file which will contain the extracted descriptors
% @return: returns the number of extracted descriptors
function descriptors = extract_features(listFileName, groundTruthFileName, upperLimitToDescriptorsCount, outputFileName)
	global DESCRIPTOR_SIZE
	
	descriptorsCount = 0;
	%fout = fopen(outputFileName, 'w');
	listFile = fopen(listFileName, 'r');
	groundTruthFile = fopen(groundTruthFileName, 'r');

	%overwriting old data!
	tmp = fopen(outputFileName, 'w');
	fclose(tmp);

	descriptors = [];
	
	while ~feof(listFile)
		imgPath = strtrim(fgets(listFile)); % read training file names. one filename per line
		if groundTruthFile ~= -1
			boundingBoxesLine = strtrim(fgets(groundTruthFile));
		else
			boundingBoxesLine = '';
		end
		
		[left, top, right, bottom] = getBoundingBoxes(boundingBoxesLine);
		
		ipts = extractImageFeatures(imgPath, left, top, right, bottom,  true);

		crntDescriptors = zeros(length(ipts), DESCRIPTOR_SIZE);
		for i=1:length(ipts)
			crntDescriptors(i, :) = ipts(i).descriptor;
		end
		
		print_descriptor(crntDescriptors, outputFileName, 'a');
		descriptorsCount = descriptorsCount + length(ipts);
		descriptors = [descriptors; crntDescriptors];

		if descriptorsCount >= upperLimitToDescriptorsCount
			break;
		end
	end
	
	fclose(listFile);
	if(groundTruthFile ~= -1)
		fclose(groundTruthFile);
	end
end


