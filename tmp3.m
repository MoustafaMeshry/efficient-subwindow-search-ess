function tmp3()
	CATEGORY = 'cat';
	POSITIVE_LIST_FILE_NAME = strcat('categories/', CATEGORY, '/test/negativeList.txt');
	CODEBOOK_FILE_NAME = strcat('categories/', CATEGORY, '/codebook.txt');
	
	fin = fopen(POSITIVE_LIST_FILE_NAME, 'r');
	codebook = csvread(CODEBOOK_FILE_NAME);
	
	while ~feof(fin)
		imgPath = strtrim(fgets(fin));
		[x, y, featureId] = getImageClusters(imgPath, 0, 0, 1000000, 1000000, true, codebook, 'null');
		fileName = getFileName(imgPath);
		fout = fopen(strcat('categories/cat/test/negative/', fileName, '.clst'), 'w');
		for i=1:length(x)
			fprintf(fout, '%d %d %d\n', int64(x(i)), int64(y(i)), featureId(i)-1); %because cluserID's are 0-based!
		end
		fclose(fout);
	end
	
	fclose(fin);
end

function fileName = getFileName(imgPath)
	b = imgPath;
	for i=1:8
		[a,b] = strtok(b, '/');
	end
	[a, b] = strtok(a, '.');
	fileName = a;
end

