% This function wraps the c++ executable file "ess"
% ess usage: ./ess imagewidth imageheight weight-file data-file
% ess output: 
% Output is a single line with all detected box in the format
% SCORE1 LEFT1 TOP1 RIGHT1 BOTTOM1 [SCORE2 LEFT2 TOP2 RIGHT2 BOTTOM2 ... SCOREn LEFTn TOPn RIGHTn BOTTOMn]
%
% this fucntion returns 5 vectors. each vector is of size n
% socre = [SCORE1, SCORE2, ... SCOREn];
% left = [LEFT1, LEFT2, ... LEFTn];
% top = [TOP1, TOP2, ... TOPn];
% right = [RIGHT1, RIGHT2 ... RIGHTn]
% bottom = [BOTTOM1, BOTTOM2 ... BOTTOMn]

function result = ess_wrapper (options, imagePath, weightFile, dataFile)
	img = imread(imagePath);
	[imgHeight, imgWidth, tmp] = size(img);
	[status, output] = system([options, ' ./myess ', num2str(imgWidth), ' ', num2str(imgHeight), ' ', weightFile, ' ', dataFile]);
	score  = [];
	left   = [];
	top    = [];
	right  = [];
	bottom = [];
	
%	while ~isempty(output)
		[token, output] = strtok(output);		
		score = [score, str2double(token)];
		[token, output] = strtok(output);
		left = [left, str2num(token)];
		[token, output] = strtok(output);
		top = [top, str2num(token)];
		[token, output] = strtok(output);
		right = [right, str2num(token)];
		[token, output] = strtok(output);
		bottom = [bottom, str2num(token)];
%	end
	
%	result = {score; left; top; right; bottom};
	result = zeros(5, 1);
	result(1) = score;
	result(2) = left;
	result(3) = top;
	result(4) = right;
	result(5) = bottom;
end
