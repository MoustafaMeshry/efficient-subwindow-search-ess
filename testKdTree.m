function testKdTree()
	codebook = csvread('categories/cat/codebook.txt');
	kdTree = textread('categories/cat/kdTree.txt', '%d');
	kdTree = kdTree(2:length(kdTree));

	errCnt = 0;
	for i=1:size(codebook, 1)
		id = findClosestMatch(codebook(i,:), codebook, kdTree);
		if id~=i
			errCnt = errCnt + 1;
		end
	end
	
	fprintf('Error Count = %d\n', errCnt);
end

function featureId = findClosestMatch(targetDescriptor, codebook, tree)
	global DESCRIPTOR_SIZE
	DESCRIPTOR_SIZE = 64;

	featureId = tree(1);
	treeIndex = 1;
	compDimension = 1;
	minSse = 1000000000000000000;
	
	while treeIndex <= length(tree) && tree(treeIndex) ~= -1
		sse = sumSquaredError(targetDescriptor, codebook(tree(treeIndex), :));
		if sse < minSse
			minSse = sse;
			featureId = tree(treeIndex);
		end
		
		comp = compareDescriptors(targetDescriptor, codebook(tree(treeIndex),:), compDimension);
		if comp <= 0
			treeIndex = treeIndex*2;
		else
			treeIndex = (treeIndex*2) + 1;
		end
		
		compDimension = compDimension+1;
		if(compDimension > DESCRIPTOR_SIZE)
			compDimension = 1;
		end
	end
end

function sse = sumSquaredError(descriptor1, descriptor2)
	sse = 0;
	for i=1:length(descriptor1)
		sse = sse + (descriptor1(i) - descriptor2(i))^2;
	end
end

function comp = compareDescriptors(desc1, desc2, compDimension)
	EPS = 1e-10;
	comp = 0;
	if desc1(compDimension) > desc2(compDimension) + EPS
		comp = 1;
	elseif desc1(compDimension) + EPS < desc2(compDimension)
		comp = -1;
	end
end


