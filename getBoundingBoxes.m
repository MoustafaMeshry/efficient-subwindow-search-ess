% This function parses the ground truth file (created by a domestic C program), it retrieves ALL bounding boxes for some image (each line represents bounding boxes for an image)
% boundingBoxLine: one line from the ground truth file, representing bounding boxes of an image
% @return: returns four column vectors representing the all bounding boxes in the image. The length of any vectors is the same as the number of bounding boxes
function [left, top, right, bottom] = getBoundingBoxes(boundingBoxLine)
		if length(boundingBoxLine) == 0
			left = 0;
			top = 0;
			right = 1000000000;
			bottom = 1000000000;
			return;
		end
		
		toks = strsplit(boundingBoxLine, ';');
		n = length(toks);
		left = zeros(n, 1);
		top = zeros(n, 1);
		right = zeros(n, 1);
		bottom = zeros(n, 1);
		
		for i=1:n
			rect = strsplit(char(toks(i)), ',');
			left(i) = str2num(char(rect(1)));
			top(i) = str2num(char(rect(2)));
			right(i) = str2num(char(rect(3)));
			bottom(i) = str2num(char(rect(4)));
		end
end

