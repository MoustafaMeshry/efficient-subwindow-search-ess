% descriptors: array of descriptors to be printed
% filename: output file name
% writemMode: 'w' for creating a new file (or overwriting existing ones), 'a' for appending to files
function print_descriptor(descriptors, filename, writeMode)
	cnt = 0;
	fout = fopen(filename, writeMode);
	for i=1:size(descriptors, 1)
		for j=1:size(descriptors, 2)
			if(j>1) 
				fprintf(fout, ',');
			end
			fprintf(fout, '%g', descriptors(i, j));
		end
		fprintf(fout, '\n');
	end
	fclose(fout);
end
