#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <vector>

using namespace std;

struct Feature{
	int x, y, clstId;
	Feature(int x, int y, int clstId) : x(x), y(y), clstId(clstId) {}
	
	Feature(){
		x = y = clstId = -1;
	}
};

int main(int argc, char* argv[]){
	DIR *dir;
	struct dirent *ent;
	char filePath[200], *name, *extension, imgName[20], line[1000];
	char *dirPath = argv[1];

	if((dir = opendir(dirPath)) == NULL){
		printf("invalid directory path!\n");
		printf("usage: ./clstFix.o <directory path>\n");
		return 1;
	}
	
	while ((ent = readdir(dir)) != NULL) {
		name = strtok(ent->d_name, ".");
		
		//skipping hidden and temporary files
		if(!name || ent->d_name[0] == '.' || ent->d_name[0] == '~') continue;

		extension = strtok(NULL, ".\n");
		if(strcmp(extension, "clst")) continue;
		
		sprintf(filePath, "%s/%s.clst", dirPath, name);
		printf("fixing file: \'%s\'\n", filePath);

		FILE *fin = fopen(filePath, "r");
		
		Feature f;
		vector<Feature> vec;
		
		while(!feof(fin) && fgets(line, 1000, fin)){
			sscanf(line, "%d %d %d", &f.x, &f.y, &f.clstId);
			vec.push_back(f);			
		}
		
		fclose(fin);
		
		FILE *fout = fopen(filePath, "w");
		for(int i=0; i<(int) vec.size(); ++i){
			fprintf(fout, "%d %d %d\n", vec[i].x, vec[i].y, vec[i].clstId-1);
		}
		fclose(fout);
	}

	closedir (dir);
	
	return 0;
}

