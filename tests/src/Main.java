import java.io.*;
import java.util.*;

public class Main {

	void reverseClstFile()throws IOException{
		BufferedReader bf = new BufferedReader(new FileReader("1.clst"));
		PrintWriter pw = new PrintWriter(new FileWriter("1_r.clst"));
		String line;
		while((line = bf.readLine()) != null){
			String[] toks = line.split("[ ]+");
			int x = Integer.parseInt(toks[0]);
			int y = Integer.parseInt(toks[1]);
			int featureId = Integer.parseInt(toks[2]);
			pw.println(y + " " + x + " " + featureId);
		}
		
		bf.close();
		pw.close();
	}
	
	void validatePositiveTraining()throws IOException{
		BufferedReader bf = new BufferedReader(new FileReader("cat_trainval.txt"));
		ArrayList<String> them = new ArrayList<String>();
		String line = null;
		String[] toks;
		while((line = bf.readLine()) != null){
			toks = line.split("[ ]+");
			if(Integer.parseInt(toks[1])==1) them.add(toks[0]);
		}
		bf.close();
		
		ArrayList<String> us = new ArrayList<String>();
		bf = new BufferedReader(new FileReader("positiveList.txt"));
		while((line = bf.readLine()) != null){
			toks = line.split("/");
			toks = toks[toks.length-1].split("\\.");
			us.add(toks[0]);
		}
		bf.close();
		
		System.out.println("us size = " + us.size());
		System.out.println("them size = " + them.size());
		Collections.sort(us);
		Collections.sort(them);
		for(int i=0; i<them.size(); i++){
			if(!us.get(i).equals(them.get(i))){
				System.out.println(i + ") " + us.get(i) + ", " + them.get(i));
				return;
			}
		}
	}
	
	void run()throws IOException{
//		validatePositiveTraining();
		reverseClstFile();
	}
	
	public static void main(String[] args)throws IOException {
		new Main().run();
	}
}
