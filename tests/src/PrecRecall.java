import java.io.*;
import java.util.*;

public class PrecRecall {
	static int threshold = 35;
	
	public static void main(String[] args)throws IOException {
		ArrayList<Entry> arr = new ArrayList<Entry>();
		BufferedReader bf = new BufferedReader(new FileReader("../categories/cat/test/positiveResults.txt"));
		String line;
		String[] toks;
		
		int tp = 0, fp = 0, tn = 0, fn = 0, p=0, n=0;
		
		double sumPosScore = 0;
		while((line = bf.readLine()) != null){
			p++;
			toks = line.split("[ ]+");
			int result = Integer.parseInt(toks[1]);
			double score = Double.parseDouble(toks[2]);
			if(score != score){
				score = 0;
			}
			if(score >= threshold) result = 1;
			else result = 0;
			
			sumPosScore += score;
			
			if(result == 0){
				result = -1;
				fn++;
			} else{
				tp++;
			}
			arr.add(new Entry(toks[0], 1, result));
		}
		bf.close();
		System.out.println("threshold = " + threshold);
		System.out.println("positive avg = " + (sumPosScore/p));
		
		bf = new BufferedReader(new FileReader("../categories/cat/test/negativeResults.txt"));
		double sum = 0;
		int cnt = 0;
		while((line = bf.readLine()) != null){
			n++;
			toks = line.split("[ ]+");
			int result = Integer.parseInt(toks[1]);
			double score = Double.parseDouble(toks[2]);
			if(score < threshold) result = 1;
			else result = 0;
			if(result==0){
				result = 1;
				fp++;
			} else{
				result = -1;
				tn++;
			}
			
			sum+= score;
			cnt++;
			arr.add(new Entry(toks[0], -1, result));
		}
		
		System.out.println("negative avg = " + (sum/cnt));
		double recall = tp*100.0/(tp+fn);
		double precision = tp*100.0/(tp+fp);
		
		System.out.printf("precision = %.2f\n", precision);
		System.out.printf("recall = %.2f\n", recall);
		bf.close();

		Collections.sort(arr);
		PrintWriter pw = new PrintWriter(new FileWriter("prec-recall2.txt"));
		for(Entry e : arr){
			pw.println(e.actual + " " + e.result);
		}
		pw.close();
	}
	
	private static class Entry implements Comparable<Entry>{
		String fileName;
		int actual;
		int result;
		
		public Entry(String fileName, int actual, int result){
			this.fileName = fileName;
			this.actual = actual;
			this.result = result;
		}
		
		@Override
		public int compareTo(Entry e) {
			return Integer.parseInt(fileName) - Integer.parseInt(e.fileName);
		}
		
	}
}
