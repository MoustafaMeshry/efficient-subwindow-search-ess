% Constructs the histogram describing an image, it extracts all features (interest points) in the image and maps them to the codebook features
% imagePaht: path to the target image
% codebook: codebook of representative visual words
% kdtree: constructed kd-tree used to efficiently mapping image features to visual words in the codebook
% @return: A column vector of size 1000 (size of codebook), representing the histogram of the image.
function histogram = getImageHistogram(imagePath, left, top, right, bottom, isInsideBoxes, codebook, kdtree)
	[x, y, featureId] = getImageClusters(imagePath, left, top, right, bottom, isInsideBoxes, codebook, kdtree);
	numPoints = length(featureId);
	histogram = zeros(size(codebook, 1), 1);
	
	for i=1:numPoints
		histogram(featureId(i)) = histogram(featureId(i)) + 1;
	end
	
	if numPoints ~= 0
		%normalizing the histogram
		for i=1:length(histogram)
			histogram(i) = histogram(i)/numPoints;
		end
	end
end

