for i=1:45
	h = getImageHistogram(strcat('sampleImages/', num2str(i), '.png'), 0, 0, 1000000000, 1000000000, true, codebook, 'NULL');
	hists(i, :) = h';
end
output = svmclassify(svmModel, hists);

fin = fopen('categories/cat/positiveList.txt', 'r');
hists2 = zeros(387, 1000);
for i=1:387
	imgPath = strtrim(fgets(fin));
	h = getImageHistogram(imgPath, 0, 0, 1000000000, 1000000000, true, codebook, 'NULL');
	hists2(i, :) = h';
end
output2 = svmclassify(svmModel, hists2);

