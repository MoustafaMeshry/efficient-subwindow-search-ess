#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>

int main(){
	char *CATEGORY = "cat"; //must be in small letters..
//	char *ANNOTATIONS_PATH = "../../../datasets/VOCdevkit_train/VOC2006/Annotations";
//	char *IMAGES_PATH = "../../../datasets/VOCdevkit_train/VOC2006/PNGImages";
	char *ANNOTATIONS_PATH = "../../../datasets/VOCdevkit_test/VOC2006/Annotations";
	char *IMAGES_PATH = "../../../datasets/VOCdevkit_test/VOC2006/PNGImages";


	char positiveListFileName[1000], groundTruthFileName[1000], negativeListFileName[1000];
	char categoryKeyWord[64];
	sprintf(categoryKeyWord, "PAS%s", CATEGORY);
	
//	sprintf(positiveListFileName, "categories/%s/positiveList.txt", CATEGORY);
//	sprintf(negativeListFileName, "categories/%s/negativeList.txt", CATEGORY);
//	sprintf(groundTruthFileName, "categories/%s/groundTruth.txt", CATEGORY);

	sprintf(positiveListFileName, "categories/%s/test/positiveList.txt", CATEGORY);
	sprintf(negativeListFileName, "categories/%s/test/negativeList.txt", CATEGORY);
	sprintf(groundTruthFileName, "categories/%s/test/groundTruth.txt", CATEGORY);


	FILE *positiveListFile = fopen(positiveListFileName, "w");
	FILE *negativeListFile = fopen(negativeListFileName, "w");
	FILE *groundTruthFile = fopen(groundTruthFileName, "w");
	
	DIR *dir;
	struct dirent *ent;
	char filePath[200], *name, imgName[20], line[1000];

	if((dir = opendir(ANNOTATIONS_PATH)) == NULL){
		printf("invalid directory path!\n");
		return 1;
	}
	
	while ((ent = readdir(dir)) != NULL) {
		name = strtok(ent->d_name, ".");

		//skipping hidden and temporary files
		if(!name || ent->d_name[0] == '.' || ent->d_name[0] == '~') continue;

		sprintf(filePath, "%s/%s.txt", ANNOTATIONS_PATH, name);
		printf("parsing file: \'%s\'\n", filePath);

		FILE *fin = fopen(filePath, "r");
		char first = 1, found;
		int left, top, right, bottom;

		found = 0;
		while(!feof(fin) && fgets(line, 1000, fin)){
//			fgets(line, 1000, fin);
			if(strstr(line, categoryKeyWord)){
				if(!found){
					found = 1;
					fprintf(positiveListFile, "%s/%s.png\n", IMAGES_PATH, name);
				} else{
					fgets(line, 1000, fin);
					fgets(line, 1000, fin);
					char *tok = strtok(line, ":");
					tok = strtok(NULL, "\n");
					sscanf(tok, " (%d, %d) - (%d, %d)", &left, &top, &right, &bottom);
					
					if(!first) fprintf(groundTruthFile, ";");
					else first=0;
					
					fprintf(groundTruthFile, "%d,%d,%d,%d", left, top, right, bottom);
				}			
			}
		}
		
		if(found) fprintf(groundTruthFile, "\n");
		else fprintf(negativeListFile, "%s/%s.png\n", IMAGES_PATH, name);

		fclose(fin);
	}

	closedir (dir);
	
	fclose(positiveListFile);
	fclose(negativeListFile);
	fclose(groundTruthFile);

	return 0;
}

