function testVOC2006(negThreshold)

	listFile = fopen('categories/cat/test/positiveList.txt', 'r');

	groundTruthFile = fopen('categories/cat/test/groundTruth.txt', 'r');

	weightFilePath = strcat('categories/cat/cat.weight');

	imgsPaths = {};
	groundTruthLines = {};
	while ~feof(listFile)
		imgPath = strtrim(fgets(listFile));
		imgsPaths = [imgsPaths; {imgPath}];
		boundingBoxes = strtrim(fgets(groundTruthFile));
		groundTruthLines = [groundTruthLines, {boundingBoxes}];
	end
	fclose(listFile);
	fclose(groundTruthFile);
	
	allResultsFile = fopen('categories/cat/test/allResults.txt', 'w');
%%{	
	positiveResultsFile = fopen('categories/cat/test/positiveResults.txt', 'w');

	sumPosScore = 0;
	cntPosMatch = 0;
	for i=1:length(imgsPaths)
		imgPath = char(imgsPaths(i));
		fileName = getFileName(imgPath);
		dataFilePath = strcat('categories/cat/test/positive/', fileName, '.clst');
		ret = ess('', imgPath, weightFilePath, dataFilePath);
		
		score = ret(1);
		sumPosScore = sumPosScore + score;
		left = ret(2);
		top = ret(3);
		right = ret(4);
		bottom = ret(5);
		match = validate(left, top, right, bottom, groundTruthLines(i));
		if match == true% && score >= 25
			cntPosMatch = cntPosMatch + 1;
		end

		fprintf(positiveResultsFile, '%s %d %g %g %g %g %g\n', char(fileName), match, score, left, top, right, bottom);
		fprintf(allResultsFile, '%s 1 %d\n', char(fileName), match);
	end

	fclose(positiveResultsFile);
	
	fprintf('Positive Results: %d out of %d were correctly localized. Percentage = %g\n', cntPosMatch, length(imgsPaths), (cntPosMatch * 100.0/length(imgsPaths)));
	fprintf('average +ve score = %g\n', (sumPosScore/length(imgsPaths)));
%}

%------------------------------------------------------------------------------------------------------

%%{
	listFile = fopen('categories/cat/test/negativeList.txt', 'r');
	imgsPaths = {};
	while ~feof(listFile)
		imgPath = strtrim(fgets(listFile));
		imgsPaths = [imgsPaths; {imgPath}];
	end
	fclose(listFile);
	
	negativeResultsFile = fopen('categories/cat/test/negativeResults.txt', 'w');
	
%	negThreshold = 25;
	
	sumNegScore = 0;
	cntNegMatch = 0;
	for i=1:length(imgsPaths)
		imgPath = char(imgsPaths(i));
		fileName = getFileName(imgPath);
		dataFilePath = strcat('categories/cat/test/negative/', fileName, '.clst');
		ret = ess('', imgPath, weightFilePath, dataFilePath);
		score = ret(1);
		sumNegScore = sumNegScore + score;
		left = ret(2);
		top = ret(3);
		right = ret(4);
		bottom = ret(5);
		
		if score < negThreshold
			match = 1;
			cntNegMatch = cntNegMatch + 1;
		else
			match = 0;
		end
		
		fprintf(negativeResultsFile, '%s %d %g %g %g %g %g\n', char(fileName), match, score, left, top, right, bottom);
		fprintf(allResultsFile, '%s -1 %d\n', char(fileName), match);
	end

	fprintf('Negative Results: %d out of %d scored below %g. Percentage = %g\n', cntNegMatch, length(imgsPaths), negThreshold, (cntNegMatch * 100.0/length(imgsPaths)));
	fprintf('average -ve score = %g\n', (sumNegScore/length(imgsPaths)));

	
	fclose(negativeResultsFile);
	fclose(allResultsFile);	
%}
end


function fileName = getFileName(imgPath)
	b = imgPath;
	for i=1:8
		[a,b] = strtok(b, '/');
	end
	[a, b] = strtok(a, '.');
	fileName = a;
end


function match = validate(retLeft, retTop, retRight, retBottom, boundingBoxesLine)
	[left, top, right, bottom] = getBoundingBoxes(char(boundingBoxesLine));
	retArea = (retRight - retLeft) * (retBottom - retTop);
	retRect = getRectangle(retLeft, retTop, retRight, retBottom);
	
	intersectingArea = 0;

	for i=1:length(left)
		rect = getRectangle(left(i), top(i), right(i), bottom(i));
		intersectingArea = intersectingArea + rectint(retRect, rect);
	end
	
	if intersectingArea*2 >= retArea
		match = true;
	else
		match = false;
	end
end

function rect = getRectangle(left, top, right, bottom)
	rect = zeros(1, 4);
	rect(1) = left;
	rect(2) = top;
	rect(3) = (right-left);
	rect(4) = (bottom-top);
end


