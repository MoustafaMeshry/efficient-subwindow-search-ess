function test(index)
	global POSITIVE_LIST_FILE_NAME CATEGORY
	
%	fin = fopen(POSITIVE_LIST_FILE_NAME, 'r');
	fin = fopen('categories/cat/test/positiveList.txt', 'r');
%	fin = fopen('categories/cat/test/negativeList.txt', 'r');
%	fin = fopen('categories/cat/negativeList.txt', 'r');


	weightFilePath = strcat('categories/', CATEGORY, '/', CATEGORY, '.weight');

	imgsPaths = {};
	while ~feof(fin)
		imgPath = strtrim(fgets(fin));
		imgsPaths = [imgsPaths; {imgPath}];
	end

	imgPath = char(imgsPaths(index));
	fileName = getFileName(imgPath);

%	dataFilePath = strcat('categories/', CATEGORY, '/train/positive/', fileName, '.clst');
	dataFilePath = strcat('categories/', CATEGORY, '/test/positive/', fileName, '.clst');
%	dataFilePath = strcat('categories/', CATEGORY, '/test/negative/', fileName, '.clst');
%	dataFilePath = strcat('categories/', CATEGORY, '/train/negative/', fileName, '.clst');

	ess('', imgPath, weightFilePath, dataFilePath);
	
%{	
	for i=1:length(imgsPaths)
		imgPath = char(imgsPaths(i));
		fileName = getFileName(imgPath);
		dataFilePath = strcat('categories/', CATEGORY, '/train/', fileName, '.clst');
		ess('', imgPath, weightFilePath, dataFilePath);
		break;
	end
%}
	fclose(fin);
end

function fileName = getFileName(imgPath)
	b = imgPath;
	for i=1:8
		[a,b] = strtok(b, '/');
	end
	[a, b] = strtok(a, '.');
	fileName = a;
end

