function chooseHardNegatives()
	global POSITIVE_LIST_FILE_NAME CATEGORY
	
	fin = fopen('categories/cat/negativeList.txt', 'r');
	weightFilePath = strcat('categories/', CATEGORY, '/', CATEGORY, '.weight');

	imgsPaths = {};
	while ~feof(fin)
		imgPath = strtrim(fgets(fin));
		imgsPaths = [imgsPaths; {imgPath}];
	end
	fclose(fin);

	hardNegatives = {};
	for i=1:length(imgsPaths)
		imgPath = char(imgsPaths(i));
		fileName = getFileName(imgPath);
		dataFilePath = strcat('categories/', CATEGORY, '/train/negative/', fileName, '.clst');
		ret = ess('', imgPath, weightFilePath, dataFilePath);
		
		if ret(1) > 45
			hardNegatives = [hardNegatives; {imgPath}];
%			if length(hardNegatives) == 400
%				break;
%			end
		end
	end
	
	fout = fopen('categories/cat/hardNegatives.txt', 'w');
	for i=1:length(hardNegatives)
		fprintf(fout, '%s\n', char(hardNegatives(i)));
	end	
	fclose(fout);
end

function fileName = getFileName(imgPath)
	b = imgPath;
	for i=1:8
		[a,b] = strtok(b, '/');
	end
	[a, b] = strtok(a, '.');
	fileName = a;
end



