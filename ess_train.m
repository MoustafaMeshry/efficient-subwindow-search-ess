function ess_train()

	global MAX_NUM_DESCRIPTORS DESCRIPTORS_COUNT_UPPER_LIMIT CODEBOOK_SIZE DESCRIPTOR_SIZE CATEGORY POSITIVE_LIST_FILE_NAME NEGATIVE_LIST_FILE_NAME GROUND_TRUTH_FILE_NAME NUM_DESCRIPTORS POSITIVE_DESCRIPTORS_FILE_NAME NEGATIVE_DESCRIPTORS_FILE_NAME CODEBOOK_FILE_NAME NEGATIVE_TRAINING_FILE_NAME CATEGORIES
	
	initGlobalVars();

	%TODO call the C code that prepares the input files for your matlab files

%	NUM_DESCRIPTORS = extractFeatures();

%	randomSample = sampleDescriptors(POSITIVE_DESCRIPTORS_FILE_NAME, NEGATIVE_DESCRIPTORS_FILE_NAME, NUM_DESCRIPTORS);	
%	randomSample = csvread(strcat('categories/', CATEGORY, '/sampledDescriptors.txt'));

%	codebook = constructCodebook(randomSample, CODEBOOK_SIZE);	
	codebook = csvread(CODEBOOK_FILE_NAME);
	
%	constructKdTree(codebook);
	kdTree = loadKdTree();

%%%%%% 
hardListFile = 'categories/cat/hardNegatives.txt';
%%%%%% 
hardOutputFile = 'categories/cat/hardNegativesHistograms.txt';
%%%%%% generateTrainingHistograms(hardListFile, '', true, codebook, kdTree, hardOutputFile, 'w', 1000000000);

% -------------------------------------------- <GENERATE TRAINING HISTOGRAMS> ------------------------------------------------
% generate +ve training histograms based on cats' bounding boxes. 	Each bounding box is a +ve training example
%	numPosHistograms = generateTrainingHistograms(POSITIVE_LIST_FILE_NAME, GROUND_TRUTH_FILE_NAME, true, codebook, kdTree, strcat('categories/', CATEGORY, '/positiveTrainingHistograms.txt'), 'w', 1000000000);

%	fprintf('Number of +ve training histograms = %d\n', numPosHistograms);
	
%	sampledNegativeListFileName = strcat('categories/', CATEGORY, '/sampledNegativeList.txt');
%	sampledNegativeGroundTruthFileName = strcat('categories/', CATEGORY, '/sampledNegativeGroundTruth.txt');

%	sampleNegativeTrainingExamples(numPosHistograms, sampledNegativeListFileName, sampledNegativeGroundTruthFileName);
	
% generate -ve training histograms from -ve training examples
%	numNegHistograms = generateTrainingHistograms(sampledNegativeListFileName, sampledNegativeGroundTruthFileName, true, codebook, kdTree, strcat('categories/', CATEGORY, '/negativeTrainingHistograms.txt'), 'w', 1000000000);
	
% generate -ve training histograms from regions outside bounding boxes in +ve training examples	
%	numNegHistograms2 = generateTrainingHistograms(POSITIVE_LIST_FILE_NAME, GROUND_TRUTH_FILE_NAME, false, codebook, kdTree, strcat('categories/', CATEGORY, '/negativeTrainingHistograms.txt'), 'a', 1000000000);
	
%	numNegHistograms = numNegHistograms + numNegHistograms2;
	
%	fprintf('Number of -ve training histograms = %d\n', numNegHistograms);
	
% -------------------------------------------- </GENERATE TRAINING HISTOGRAMS> ------------------------------------------------

	svmModel = trainClassifier();
%	load(strcat('categories/', CATEGORY, '/svmModel.mat'),'svmModel');
	
	calculateWeightsFile(svmModel);

	%TODO call function to calculate *.clst files
end

function initGlobalVars()
	global MAX_NUM_DESCRIPTORS DESCRIPTORS_COUNT_UPPER_LIMIT CODEBOOK_SIZE DESCRIPTOR_SIZE CATEGORY POSITIVE_LIST_FILE_NAME NEGATIVE_LIST_FILE_NAME GROUND_TRUTH_FILE_NAME NUM_DESCRIPTORS POSITIVE_DESCRIPTORS_FILE_NAME NEGATIVE_DESCRIPTORS_FILE_NAME CODEBOOK_FILE_NAME NEGATIVE_TRAINING_FILE_NAME CATEGORIES

	MAX_NUM_DESCRIPTORS = 50000;
	DESCRIPTORS_COUNT_UPPER_LIMIT = 1000000;
	CODEBOOK_SIZE = 1000;
	DESCRIPTOR_SIZE = 64;
	CATEGORY = 'cat';
	POSITIVE_LIST_FILE_NAME = strcat('categories/', CATEGORY, '/positiveList.txt');
	NEGATIVE_LIST_FILE_NAME = strcat('categories/', CATEGORY, '/negativeList.txt');
	GROUND_TRUTH_FILE_NAME = strcat('categories/', CATEGORY, '/groundTruth.txt');
	NUM_DESCRIPTORS = MAX_NUM_DESCRIPTORS;
	POSITIVE_DESCRIPTORS_FILE_NAME = strcat('categories/', CATEGORY, '/positiveDescriptorsDump.txt');
	NEGATIVE_DESCRIPTORS_FILE_NAME = strcat('categories/', CATEGORY, '/negativeDescriptorsDump.txt');
	CODEBOOK_FILE_NAME = strcat('categories/', CATEGORY, '/codebook.txt');
	NEGATIVE_TRAINING_FILE_NAME = strcat('categories/', CATEGORY, '/negativeTrainingExamples.txt');
	CATEGORIES = {'bicycle', 'bus', 'car', 'cat', 'cow', 'dog', 'horse', 'motorbike', 'person', 'sheep'};
end


function numDescriptors = extractFeatures()
	global MAX_NUM_DESCRIPTORS DESCRIPTORS_COUNT_UPPER_LIMIT POSITIVE_LIST_FILE_NAME NEGATIVE_LIST_FILE_NAME GROUND_TRUTH_FILE_NAME POSITIVE_DESCRIPTORS_FILE_NAME NEGATIVE_DESCRIPTORS_FILE_NAME
	
	%Extracting features (descriptors)
	'Extracting features (descriptors)'
	positiveDescriptors = extract_features(POSITIVE_LIST_FILE_NAME, GROUND_TRUTH_FILE_NAME, DESCRIPTORS_COUNT_UPPER_LIMIT, POSITIVE_DESCRIPTORS_FILE_NAME);
	totalPositiveDescriptors = size(positiveDescriptors, 1);
	
	negativeDescriptors = extract_features(NEGATIVE_LIST_FILE_NAME, 'NULL', DESCRIPTORS_COUNT_UPPER_LIMIT, NEGATIVE_DESCRIPTORS_FILE_NAME);
	totalNegativeDescriptors = size(negativeDescriptors, 1);
	
	strcat('Features extracted successfully! Number of extracted positive descriptors = ', num2str(totalPositiveDescriptors), '. Number of extracted negative descriptors = ', num2str(totalNegativeDescriptors))

	numDescriptors = min(2*min(totalPositiveDescriptors, totalNegativeDescriptors), MAX_NUM_DESCRIPTORS);
end


function randomSample = sampleDescriptors(positiveDescriptorsFile, negativeDescriptorsFile, numDescriptors)
	%Choosing a random sample from extracted features
	global DESCRIPTOR_SIZE CATEGORY
	
	fprintf('Choosing a random sample of descriptors of size %d\n', numDescriptors);
	positiveDescriptors = csvread(positiveDescriptorsFile);
	negativeDescriptors = csvread(negativeDescriptorsFile);
	
	totalPositiveDescriptors = size(positiveDescriptors, 1);
	totalNegativeDescriptors = size(negativeDescriptors, 1);
	
	posRsIndices = sort(randsample(totalPositiveDescriptors, numDescriptors/5));
	negRsIndices = sort(randsample(totalNegativeDescriptors, 4*numDescriptors/5));
	numCols = DESCRIPTOR_SIZE;
	randomSample = zeros(numDescriptors, numCols);
	
	pos_rs = fopen(strcat('categories/', CATEGORY, '/rs_positive'), 'w');
	neg_rs = fopen(strcat('categories/', CATEGORY, '/rs_negative'), 'w');
	
	for i=1:length(posRsIndices)
		randomSample(i, :) = positiveDescriptors(posRsIndices(i), :);
		fprintf(pos_rs,'%5d: %07d\n', i, posRsIndices(i));
	end
	fclose(pos_rs);
	
	for i=1:length(negRsIndices)
		randomSample(i+length(posRsIndices), :) = negativeDescriptors(negRsIndices(i), :);
		fprintf(neg_rs,'%5d: %07d\n', i, negRsIndices(i));
	end
	fclose(neg_rs);

	rs = fopen(strcat('categories/', CATEGORY, '/sampledDescriptors.txt'), 'w');

	for i=1:numDescriptors
		for j=1:numCols
			if(j>1)
				fprintf(rs, ',');
			end
			fprintf(rs, '%g', randomSample(i, j));
		end
		fprintf(rs, '\n');	
	end
	fclose(rs);
	fprintf('Random sample constructed successfuly!\n');
end


function codebook = constructCodebook(descriptors, codebookSize)
	%Constructing codebook by clustering descriptors into 1000 representative visual words
	global CODEBOOK_FILE_NAME
	
	fprintf('Applying k-means to select 1000-visual word codebook\n');
	options = statset;
	options.MaxIter = 700;
	[clstIndex, centroid] = kmeans(descriptors, codebookSize, 'options', options);
	codebook = centroid;
	fprintf('k-means clustering completed successfully!\n');
	
	%exporting the codebook (dictionary): the bag of words features..
	fprintf('Exporting the codebook (dictionary) of bag of words features\n');
	fout = fopen(CODEBOOK_FILE_NAME, 'w');
	for i=1:size(centroid, 1)
		for j=1:size(centroid,2)
			if j>1
				fprintf(fout,',');
			end
			fprintf(fout,'%g', centroid(i,j));
		end
		fprintf(fout, '\n');
	end
	fclose(fout);
end


function constructKdTree(codebook)
	%constructing k-dTree from the codebook	
	javaaddpath k-dTree/bin/
	kdTreeBuilder = KdTreeBuilder();
	kdTreeBuilder.buildKdTree();
end

function tree = loadKdTree()
	global CATEGORY
			
	fin = fopen(strcat('categories/', CATEGORY, '/kdTree.txt'), 'r'); 

	line = strtrim(fgets(fin));
	treeSize = str2num(line);
	tree = zeros(treeSize, 1);
	for i=1:treeSize
		line = strtrim(fgets(fin));
		tree(i) = str2num(line);
	end
end

%{
function sampleNegativeTrainingExamples(examplesCount, negativeListFile, outputFileName)
	fin = fopen(negativeListFile, 'r');
	examples = {};
	while ~feof(fin)
		ex = strtrim(fgets(fin));
		examples = [examples; {ex}];
	end
	fclose(fin);
	
	rsIndices = randsample(length(examples), examplesCount);
	rs = examples(rsIndices);
	
	fout = fopen(outputFileName, 'w');
	for i=1:examplesCount
		fprintf(fout, '%s\n', char(rs(i)));
	end
	fclose(fout);
end
%}

function sampleNegativeTrainingExamples(requiredNumber, outputListFileName, outputGroundTruthFileName)
	global CATEGORY CATEGORIES
	numCategories = length(CATEGORIES);
	categoryShare = int64(ceil(requiredNumber/(numCategories-1)));
	imgsPaths = {};
	boundingBoxes = {};
	
	for i=1:numCategories
		if strcmp(CATEGORIES(i), CATEGORY) == 0
			allPaths = textread(strcat('categories/', char(CATEGORIES(i)), '/positiveList.txt'), '%s');
			allBoxes = textread(strcat('categories/', char(CATEGORIES(i)), '/groundTruth.txt'), '%s');
			
			rsIndices = randsample(length(allPaths), categoryShare);
			rsPaths = allPaths(rsIndices);
			rsBoxes = allBoxes(rsIndices);
			
			imgsPaths = [imgsPaths; rsPaths];
			boundingBoxes = [boundingBoxes; rsBoxes];
		end
	end
	
	listFile = fopen(outputListFileName, 'w');
	groundTruthFile = fopen(outputGroundTruthFileName, 'w');
		
	for i=1:length(imgsPaths)
		fprintf(listFile, '%s\n', char(imgsPaths(i)));
		fprintf(groundTruthFile, '%s\n', char(boundingBoxes(i)));
	end
	
	fclose(listFile);
	fclose(groundTruthFile);
end


function numHistograms = generateTrainingHistograms(listFileName, groundTruthFileName, isInsideBox, codebook, kdtree, outputFileName, writeMode, maxNumHistograms)
	global CATEGORY
	
	numHistograms = 0;
	
	listFile = fopen(listFileName, 'r');
	groundTruthFile = fopen(groundTruthFileName, 'r');
	
	fout = fopen(outputFileName, writeMode);
	
	while ~feof(listFile)
		imgPath = strtrim(fgets(listFile)); % read training file names. one filename per line
		if groundTruthFile ~= -1
			boundingBoxesLine = strtrim(fgets(groundTruthFile));
		else
			boundingBoxesLine = '';
		end

		[left, top, right, bottom] = getBoundingBoxes(boundingBoxesLine);
		
		if isInsideBox == true
			for h=1:length(left)
				histogram = getImageHistogram(imgPath, left(h), top(h), right(h), bottom(h), isInsideBox, codebook, kdtree);
				numHistograms = numHistograms + 1;
		
				for i=1:length(histogram)
					if(i>1)
						fprintf(fout, ',');
					end
					fprintf(fout, '%d', histogram(i));
				end
				fprintf(fout, '\n');			
			end
		else
			histogram = getImageHistogram(imgPath, left, top, right, bottom, isInsideBox, codebook, kdtree);
			numHistograms = numHistograms + 1;
		
			for i=1:length(histogram)
				if(i>1)
					fprintf(fout, ',');
				end
				fprintf(fout, '%d', histogram(i));
			end
			fprintf(fout, '\n');			
		end
				
		if(numHistograms >= maxNumHistograms)
			break;
		end;

	end
	
	fclose(listFile);
	if groundTruthFile ~= -1
		fclose(groundTruthFile);
	end
	fclose(fout);
end


%ASSUMPTION: the training histograms are splitted exactly in half, where the first half are +ve training examples, and the 2nd half are the -ve training examples
function svmModel = trainClassifier()
	global CATEGORY
	positiveTrainingHistograms = csvread(strcat('categories/', CATEGORY, '/positiveTrainingHistograms.txt'));
	negativeTrainingHistograms = csvread(strcat('categories/', CATEGORY, '/negativeTrainingHistograms.txt'));
%%%%%%% hardNegativesHistograms = csvread(strcat('categories/', CATEGORY, '/hardNegativesHistograms.txt'));

	positiveSize = 2*size(positiveTrainingHistograms, 1);
%%%%%%% positiveSize = size(positiveTrainingHistograms, 1);
	
	%trainingHistograms = [positiveTrainingHistograms; positiveTrainingHistograms; positiveTrainingHistograms; negativeTrainingHistograms];
	trainingHistograms = [positiveTrainingHistograms; positiveTrainingHistograms; negativeTrainingHistograms];

%%%%%%% hardNegLen = size(hardNegativesHistograms, 1);
%%%%%%% trainingHistograms = [positiveTrainingHistograms; positiveTrainingHistograms; negativeTrainingHistograms; hardNegativesHistograms(1:hardNegLen/4, :)];

%%%%%%% trainingHistograms = [positiveTrainingHistograms; negativeTrainingHistograms; hardNegativesHistograms];
	
	labels = zeros(size(trainingHistograms, 1), 1);
	labels(1:positiveSize) = 1;
	labels(positiveSize + 1 : length(labels)) = -1;

%	svmModel = svmtrain(trainingHistograms, labels);
	svmModel = svmtrain(trainingHistograms, labels, 'autoscale', false);
%	svmModel = svmtrain(labels, trainingHistograms);
	save(strcat('categories/', CATEGORY, '/svmModel.mat'), 'svmModel');
	fprintf('model trained successfully!\n');
end


