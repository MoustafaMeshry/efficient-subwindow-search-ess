function ipts = extractImageFeatures(imgPath, left, top, right, bottom, isInsideBoxes)
	
	ipts = [];	

	img = imread(imgPath);
	iptsAll = OpenSurf(img);
		
	for i=1:length(iptsAll)
		included = false;
		for k=1:length(top)
			if iptsAll(i).x >= left(k) && iptsAll(i).x <= right(k) && iptsAll(i).y >= top(k) && iptsAll(i).y <= bottom(k)
				included = true;
				break;
			end	
		end
		
		if included==isInsideBoxes
			ipts = [ipts; iptsAll(i)];
		end
	end
end
